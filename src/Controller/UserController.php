<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class UserController extends AbstractController
{
    /**
     * @Route("/login", name="api_login", methods={"POST"})
     */
    public function login()
    {
        return $this->json(['result' => true]);
    }

    /**
     * @Route("/profile", name="api_profile")
     */
    public function profile()
    {
       return $this->json([
           'user' => $this->getUser()
       ]);
    }

    /**
     * @Route("/", name="api_home")
     */
    public function home()
    {
       return $this->json(['result' => true]);
    }
}
