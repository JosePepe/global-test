<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\ORM\EntityManagerInterface;
use App\Entity\Article;
use App\Repository\ArticleRepository;
use App\Entity\Comments;
use App\Repository\CommentsRepository;
use App\Repository\UserRepository;
use Symfony\Component\HttpKernel\KernelInterface;

/**
* @Route("/blog", name="blog")       
*/
class BlogController extends AbstractController
{
    private $em;
    private $currentDate;
    private $articleRepository;
    private $userReposiroty;
    private $commentsRepository;

    function __construct(EntityManagerInterface $em, ArticleRepository $articleRepository, UserRepository $userReposiroty, CommentsRepository $commentsRepository)
    {
        $this->em = $em;
        $this->currentDate = new \Datetime;
        $this->articleRepository = $articleRepository;
        $this->userReposiroty = $userReposiroty;
        $this->commentsRepository = $commentsRepository;
    }

    /**
     * @Route("/article/create", name="_create_article", methods={"POST"})
     */
    public function createArticcle(Request $request): Response
    {
        try {

            $photo = $request->files->get('photo');
            $lastArticle = $this->articleRepository->findBy(array('owner' => $request->request->get('owner') ),array('id'=>'DESC'),1,0);
            $owner = $this->userReposiroty->find($request->request->get('owner'));

            $article = new Article;
            $article->setOwner($owner);
            $article->setArticle($this->validateNullParam($request->request->get('article')));
            $article->setCreatedAt($this->currentDate);
            $article->setOrderInt(isset($lastArticle[0]) ? $lastArticle[0]->getOrderInt() + 1 : 0);

            if (isset($photo)) {

                $dir_subida = '../public/';
                $idArticle = isset($lastArticle[0]) ? $lastArticle[0]->getId() + 1 : 0;
                $fichero_subido = $dir_subida . "article_" . $idArticle . "_" . $owner->getId() . "." . $photo->getClientOriginalExtension();

                if (move_uploaded_file($photo->getPathname(), $fichero_subido)) {
                    $article->setPhoto("article_" . $idArticle . "_" . $owner->getId() . "." . $photo->getClientOriginalExtension());
                } else {

                }

            }

            $this->em->persist($article);
            $this->em->flush();

            if ($article->getId()) {

                $object = $this->json([
                    'message' => 'Articulo posteado con exito.',
                    'status' => 'Ok',
                ]);
            }


        } catch (Exception $e) {
            $object = $this->json([
                'message' => $e->getMessage(),
                'status' => 'Fail',
            ]);
        }
        
        return $object;
    }

    /**
     * @Route("/article/update", name="_update_article", methods={"PUT"})
     */
    public function updateArticcle(Request $request): Response
    {
        try {
            $article = $this->articleRepository->find( $this->articleRepository->find($request->get('id')) );

            $article->setArticle($this->validateNullParam($request->get('article')));

            $this->em->persist($article);
            $this->em->flush();

            if ($article->getId()) {

                $object = $this->json([
                    'message' => 'Articulo actualizado con exito.',
                    'status' => 'Ok',
                ]);
            }


        } catch (Exception $e) {
            $object = $this->json([
                'message' => $e->getMessage(),
                'status' => 'Fail',
            ]);
        }
        
        return $object;
    }

    /**
     * @Route("/article/delete", name="_delete_article", methods={"DELETE"})
     */
    public function deleteArticcle(Request $request): Response
    {
        try {

            $this->em->remove( $this->articleRepository->find($request->get('id')) );
            $this->em->flush();

            $object = $this->json([
                'message' => 'Articulo eliminado con exito.',
                'status' => 'Ok',
            ]);


        } catch (Exception $e) {
            $object = $this->json([
                'message' => $e->getMessage(),
                'status' => 'Fail',
            ]);
        }
        
        return $object;
    }

    /**
     * @Route("/", name="_find_list", methods={"GET"})
     */
    public function findListArticle(Request $request): Response
    {
        try {
            $owner = $request->get('owner');
            $articles = $this->em->getRepository(Article::class)->findAll();

            $object = $this->json([
                'message' => 'Find list',
                'status' => 'OK',
                'articles' => $articles
            ]);

        } catch (Exception $e) {
            $object = $this->json([
                'message' => $e->getMessage(),
                'status' => 'Fail',
            ]);
        }
        
        return $object;
    }

    /**
     * @Route("/comment/add", name="_add_comment", methods={"POST"})
     */
    public function addComment(Request $request): Response
    {
        try {
            $article = $this->articleRepository->find($request->request->get('id'));
            $owner = $this->userReposiroty->find($request->request->get('owner'));
            $lastComment = $this->commentsRepository->findBy(array('owner' => $request->request->get('owner') ),array('id'=>'DESC'),1,0);

            $comment = new Comments;
            $comment->setOwner($owner);
            $comment->setComment($this->validateNullParam($request->request->get('comment')));
            $comment->setCreatedAt($this->currentDate);
            $comment->setOrderInt(isset($lastComment[0]) ? $lastComment[0]->getOrderInt() + 1 : 0);
            $article->addComments($comment);
            $this->em->persist($article);
            $this->em->flush();

            if ($article->getId()) {

                $object = $this->json([
                    'message' => 'Comentario posteado con exito.',
                    'status' => 'Ok',
                ]);
            }


        } catch (Exception $e) {
            $object = $this->json([
                'message' => $e->getMessage(),
                'status' => 'Fail',
            ]);
        }
        
        return $object;
    }

    public function validateNullParam($param)
    {
        $param = isset($param)&&$param != null ? $param : NULL;

        return $param;
    }
}
