<?php

namespace App\Command;

use App\Entity\User;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class CreateUserCommand extends Command
{
    protected static $defaultName = 'app:create-user';
    private $em;
    private $userRepository;
    private $userPasswordEncoder;

    function __construct(EntityManagerInterface $em, UserPasswordEncoderInterface $userPasswordEncoder, UserRepository $userRepository)
    {
        $this->em = $em;
        $this->userRepository = $userRepository;
        $this->userPasswordEncoder = $userPasswordEncoder;

        parent::__construct();
    }

    protected function configure()
    {
        $this
            ->setDescription('This command allows you to create a user')
            ->setHelp('This command allows you to create a user')
            ->addArgument('email', InputArgument::REQUIRED, 'User email')
            ->addArgument('password', InputArgument::REQUIRED, 'User password')
            ->addArgument('username', InputArgument::REQUIRED, 'User username');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $output->writeln('<fg=white;bg=cyan>User creator</>');

        $email = $input->getArgument('email');
        $plainPassword = $input->getArgument('password');
        $username = $input->getArgument('username');

        $user = $this->userRepository->findOneByEmail($email);

        if (!empty($user)) {
            $output->writeln('<error>That user already exists</error>');
            return 0;
        }

        $user = new User();
        $user->setEmail($email);
        $password = $this->userPasswordEncoder->encodePassword($user, $plainPassword);
        $user->setPassword($password);
        $user->setUsername($username);
        $this->em->persist($user);
        $this->em->flush();

        $output->writeln('<fg=white;bg=green>User created!!!</>');
        return 0;
    }
}
